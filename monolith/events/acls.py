import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_photo(city,state):
    headers ={"Authorization":PEXELS_API_KEY}
    params= {"per_page":1, "query":city + " " + state}
    url = "https://api.pexels.com/v1/search"
    r = requests.get(url, headers=headers, params=params)
    content = json.loads(r.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return{"picture_url": None}

def get_weather_data(city,state):
    us_iso_code = "ISO 3166-2-US"
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{us_iso_code}&limit=1&appid={OPEN_WEATHER_API_KEY}"
    r = requests.get(url)
    g_content = json.loads(r.content)
    location = g_content[0]
    lon= location["lon"]
    lat= location["lat"]

    weather_url =f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(weather_url)
    content = json.loads(response.content)
    try:
        return {
                "temp": str(content["main"]["temp"]) + "°F",
                "description": content["weather"][0]["description"]
            }
    except:
        return {
            "temp": None ,
            "description": None
        }
