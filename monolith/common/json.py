from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


# turns the fancy querset list into a normal list
class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    # handle locationlistencoder in events/api_views
    encoders = {}

    def default(self, o):
        # check if incoming object stored in o parameter
        # is an instance of the class stored in self.model property
        if isinstance(o, self.model):
            d = {}
            # if all objects with a method named get_api_url on them
            # should include the method by default
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()
            for property in self.properties:
                # use getattr to get the value of the object by its name
                value = getattr(o, property)
                # if property to value = encoder is to handle the
                # locationlistencoder
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                # now put the value (from getattr)
                # in the dictionary for the property name
                d[property] = value
            # escape catch: d.update(self.get_extra_data(o))
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    # escape catch
    def get_extra_data(self, o):
        return {}


# status in api_list_presentations and state abbre
# are edge cases(oddballs). These are hard to come
# up with a nice solution and rare. so you can create
# an escape hatch, though remember, most SWEs are found of them

# why does it need to be self.model?
# why do we use super() in the return super().default(o)
# may need clarification in location handler
